<?php

namespace App\Controller;

use App\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Mime\Email;

class ContactController extends AbstractController
{
    #[Route('/contact', name: 'app_contact')]
    public function index(Request $request, MailerInterface $mailer): Response
    {
        $form = $this->createForm(ContactType::class);

        $form->handleRequest($request); //Va chercher les données que l'utilisateur a fournit

        if ($form->isSubmitted() && $form->isValid()) { //Vérifie si les données sont remplis

            $data = $form->getData(); //Récupère les données
            
            $nom = $data['nom'];  //Va rechercher les données du formulaire soumit par l'utilisateur
            $prenom = $data['prenom'];
            $adress = $data['email'];
            $content = $data['content'];

            $email = (new Email())      // Permet de créer l'email
            ->from($adress)                                     //email de l'utilisateur 
            ->to('praazh@gmail.com')                             //le mail qui va recuperer le formulaire (destinataire)
            ->subject('Demande de contact')       //Le CC de l'email
            ->text($content);
            //->html('<p>See Twig integration for better HTML integration!</p>');  Permet de mettre des messages personnalisés en html css 

            $mailer->send($email);                              // Permet l'envoie de l'email
        } 

        return $this->render('contact/index.html.twig', [
            'controller_name' => 'ContactController',
            'formulaire' => $form
        ]);
    }
}
